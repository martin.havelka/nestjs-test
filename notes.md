# Notes

## Controllers chapter
- všechno se nejdřív pokusit dělat dekorátorem - pravděpodobně to jde
- Normálně doporučují return status code nastavit dekorátorem nad handler funkcí. Když je ale třeba poslat dynamicky něco jiného (což je potřeba často), dělá se to starým nodejs způsobem - `res.status();`
- pro kombinaci přístupů v předchozím bodě je potřeba v handler funkci použít `@Res({ passthrough: true })`

## Database connection

- TyperORM
- nejde nastavení database connection řešit lépe než v dekorátoru module? - jde - ormconfig.json soubor v root adresari