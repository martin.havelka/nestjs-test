import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from './api/clients/client.module';
import { ContractModule } from './api/contracts/contract.module';
import { TodosModule } from './api/todos/todos.module';
import { LoggerMiddleware } from './global/logger.middleware';
import { AuthModule } from './authentication/auth.module';

@Module({
  imports: [TypeOrmModule.forRoot(), ClientModule, ContractModule, TodosModule, AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('*');
  }
}
