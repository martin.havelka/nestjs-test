import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ClientService } from 'src/api/clients/client.service';

@Injectable()
export class AuthService {
  constructor(
    private clientsService: ClientService,
    private jwtService: JwtService
    ) {}

  async validateUser(id: string, lastName: string): Promise<any> {
    const client = await this.clientsService.findOne(id);
    if (client && client.lastName === lastName) {
      const { lastName, ...result } = client;
      return result;
    }
    return null;
  }

  // creates jwt token - assumes successful credential checking
  async login(user: any) {
    const payload = { firstName: user.firstName, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}