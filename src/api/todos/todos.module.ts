import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TodosController } from './todos.controller';

@Module({
  imports: [HttpModule],
  providers: [],
  controllers: [TodosController],
})
export class TodosModule {}