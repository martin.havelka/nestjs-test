import { HttpService } from '@nestjs/axios';
import { Controller, Get } from '@nestjs/common';
import { response } from 'express';
import { map } from 'rxjs';

@Controller('api/todos')
export class TodosController {
    constructor(private http: HttpService) {}

    url: string = "https://jsonplaceholder.typicode.com/todos";

    @Get('')
    async getTodos(): Promise<any> {
        return this.http.get(this.url)
        .pipe(
            map(response => response.data)
        );
    }
}
