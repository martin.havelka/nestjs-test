import { IsInt, IsNumber, IsString } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Contracts {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @Column()
  contractName: string;

  @IsNumber()
  @Column("double")
  payment: number;

  @IsInt()
  @Column()
  clientId: number;
}