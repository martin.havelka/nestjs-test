import { Controller, Get, Post, Put, Param, Delete, Body, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/authentication/jwt-auth.guard';
import { ValidationPipe } from 'src/global/bodyValidation.pipe';
import { Contracts } from './contract.entity';
import { ContractService } from './contract.service';

@Controller('api/contracts')
@UseGuards(JwtAuthGuard)
export class ContractController {
    constructor(private contract: ContractService) {}
    
    @Get('')
    async getAll(): Promise<any> {
        return await this.contract.findAll();
    }

    @Get(':id')
    async getById(@Param() params): Promise<any> {
        return  await this.contract.findOne(params.id);
    }

    @Post('')
    async insert(@Body(new ValidationPipe()) body: Contracts): Promise<any> {
        return await this.contract.insertOne(body);
    }

    @Put(':id')
    async modify(@Param() params, @Body() body: Contracts): Promise<any> {
        return await this.contract.updateOne(params.id, body);
    }

    @Delete(':id')
    async delete(@Param() params): Promise<any> {
        return await this.contract.remove(params.id);
    }
}
