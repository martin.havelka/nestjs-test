import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContractController } from './contract.controller';
import { ContractService } from './contract.service';
import { Contracts } from './contract.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Contracts])],
  exports: [ContractService],
  providers: [ContractService],
  controllers: [ContractController],
})
export class ContractModule {}