import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InsertResult, Repository } from 'typeorm';
import { Contracts } from './contract.entity';


@Injectable()
export class ContractService {
    constructor(
        @InjectRepository(Contracts)
        private contractRepository: Repository<Contracts>,
      ) {}
    
      findAll(): Promise<Contracts[]> {
        return this.contractRepository.find();
      }
    
      findOne(id: string): Promise<Contracts> {
        return this.contractRepository.findOne(id);
      }

      findByClientId(id: string): Promise<Contracts[]> {
        return this.contractRepository.find({where: {clientId: id}});
      }

      insertOne(contract: Contracts): Promise<InsertResult> {
        return this.contractRepository.insert(contract);
      }
      
      updateOne(id: number, contract: Contracts): Promise<any> {
        return this.contractRepository.update({id: id}, contract);
      }

      async remove(id: string): Promise<void> {
        await this.contractRepository.delete(id);
      }
}
