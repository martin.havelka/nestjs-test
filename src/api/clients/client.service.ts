import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InsertResult, Repository } from 'typeorm';
import { Contracts } from '../contracts/contract.entity';
import { Clients } from './client.entity';
import { ContractService } from '../contracts/contract.service';


@Injectable()
export class ClientService {
    constructor(
        @InjectRepository(Clients)
        private clientRepository: Repository<Clients>, private contract: ContractService
      ) {}
    
      findAll(): Promise<Clients[]> {
        return this.clientRepository.find();
      }
    
      findOne(id: string): Promise<Clients> {
        return this.clientRepository.findOne(id);
      }

      insertOne(client: Clients): Promise<InsertResult> {
        return this.clientRepository.insert(client);
      }

      updateOne(id: number, client: Clients): Promise<any> {
        return this.clientRepository.update({id: id}, client);
      }
    
      async remove(id: string): Promise<void> {
        await this.clientRepository.delete(id);
      }

      // auth support

      findByName(firstName: string): Promise<Clients> {
        return this.clientRepository.findOne(firstName);
      }

      // join contracts function

      async getClientContracts(id: string): Promise<Clients> {
        let client: Clients = await this.findOne(id);
        let contracts: Contracts[] = await this.contract.findByClientId(id);
        
        client.contracts = contracts;
        return client;
      }
}
