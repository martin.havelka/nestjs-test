import { IsString } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Contracts } from '../contracts/contract.entity';

@Entity()
export class Clients {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @Column()
  firstName: string;

  @IsString()
  @Column()
  lastName: string;
  
  @IsString()
  @Column({ type: 'timestamp' })
  birthDate: string;

  contracts?: Contracts[];
  
  @Column()
  roles: string;
}