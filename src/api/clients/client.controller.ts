import { Controller, Get, Post, Put, Param, Delete, Body, ValidationPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/authentication/jwt-auth.guard';
import { Roles } from 'src/authorization/roles.decorator';
import { Clients } from './client.entity';
import { ClientService } from './client.service';

@Controller('api/clients')
@UseGuards(JwtAuthGuard)
export class ClientController {
    constructor(private client: ClientService) {}

    @Get('')
    @Roles("admin")
    async getAll(): Promise<any> {
        return await this.client.findAll();
    }

    @Get(':id')
    async getById(@Param() params): Promise<any> {
        return  await this.client.findOne(params.id);
    }

    @Post('')
    async insert(@Body(new ValidationPipe()) body: Clients): Promise<any> {
        return await this.client.insertOne(body);
    }

    @Put(':id')
    async modify(@Param() params, @Body() body: Clients): Promise<any> {
        return await this.client.updateOne(params.id, body);
    }

    @Delete(':id')
    async delete(@Param() params): Promise<any> {
        return await this.client.remove(params.id);
    }
}
