import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';
import { Clients } from './client.entity';
import { ContractModule } from '../contracts/contract.module';
import { ClientContractController } from './clientContract.controller';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from 'src/authorization/roles.guard';

@Module({
  imports: [TypeOrmModule.forFeature([Clients]), ContractModule],
  providers: [
    ClientService, 
    {
      provide: APP_GUARD,
      useClass: RolesGuard
    }
  ],
  controllers: [ClientController, ClientContractController],
  exports: [ClientService]
})
export class ClientModule {}