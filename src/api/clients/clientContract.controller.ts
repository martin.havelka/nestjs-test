import { Controller, Get, Param } from '@nestjs/common';
import { ClientService } from './client.service';

@Controller('api/contractsForClient')
export class ClientContractController {
    constructor(private client: ClientService) {}

    @Get(':id')
    async getById(@Param() params): Promise<any> {
        return  await this.client.getClientContracts(params.id);
    }
}
